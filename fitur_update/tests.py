from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from django.utils import timezone
from .views import index, add_post
from .models import Status
from .forms import Status_Form

class UpdateStatus(TestCase):
	def test_update_status_url_is_exist(self):
		response = Client().get('/fitur-update/')
		self.assertEqual(response.status_code, 200) #OK

	def test_update_status_using_index_func(self):
		found = resolve('/fitur-update/')
		self.assertEqual(found.func, index)

	def test_model_can_create_new_status(self):
		# Creating a new status
		new_status =  Status.objects.create(description='So I\'m here to be a winner! Go big or go home ^^')

		# Retrieving all available status
		counting_all_available_status =  Status.objects.all().count()
		self.assertEqual(counting_all_available_status, 1)

"""	def test_update_status_success_and_render_the_result(self):
		response_post = Client().post('/fitur-update/add_post',{'description': test})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/fitur-update')
		html_response =  response.content.decode('utf8')
		self.assertIn(html_response)

	def test_update_status_error_and_render_the_result(self):
		response_post = Client().post('/fitur-update/add_post', {'description': ''})
		self.assertEqual(response_post.status_code, 302)

		response = Client().get('/fitur-update')
		html_response =  response.content.decode('utf8')
		self.assertNotIn(html_response)"""