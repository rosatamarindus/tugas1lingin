from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from .forms import Status_Form
from .models import Status

# Create your views here.
response = {}
def index(request):    
	response['author'] = "Rosa Nur Rizky F.T" #TODO Implement yourname
	status = Status.objects.all()
	response['status'] = status
	response['status_form'] =  Status_Form
	html = 'update_status/update_status.html'
	return render(request, html, response)

def add_post(request):
	form = Status_Form(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['description'] = request.POST['description']
		status = Status(description=response['description'])
		status.save()
		return HttpResponseRedirect('/update-status/')
	else:
		return HttpResponseRedirect('/update-status/')