from django import forms

class  Status_Form(forms.Form):
	description_attrs = {
		'type': 'text',
		'cols': 50,
		'rows': 4,
		'class': 'status-form-textarea',
		'placeholder':'Apa yang Anda pikirkan?'
	}

	description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))